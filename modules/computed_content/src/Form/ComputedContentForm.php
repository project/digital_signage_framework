<?php

namespace Drupal\digital_signage_computed_content\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the digsig_computed_content entity edit forms.
 */
class ComputedContentForm extends ContentEntityForm {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected Renderer $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ComputedContentForm {
    /** @var \Drupal\digital_signage_computed_content\Form\ComputedContentForm $instance */
    $instance = parent::create($container);
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Exception
   */
  public function save(array $form, FormStateInterface $form_state): int {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + [
      'link' => $this->renderer->render($link),
    ];

    if ($result === SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New computed content %label has been created.', $message_arguments));
      $this->logger('digital_signage_computed_content')->notice('Created new digsig_computed_content %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The computed content %label has been updated.', $message_arguments));
      $this->logger('digital_signage_computed_content')->notice('Updated new digsig_computed_content %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.digsig_computed_content.canonical', ['digsig_computed_content' => $entity->id()]);

    return $result;
  }

}
