<?php

namespace Drupal\digital_signage_computed_content;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access control handler for the digsig_computed_content entity type.
 */
class ComputedContentAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    return match ($operation) {
      'view' => AccessResult::allowedIfHasPermission($account, 'view digsig_computed_content'),
      'update' => AccessResult::allowedIfHasPermissions($account, [
        'edit digsig_computed_content',
        'administer digsig_computed_content',
      ], 'OR'),
      default => AccessResult::neutral(),
    };

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResultInterface {
    return AccessResult::forbidden();
  }

}
