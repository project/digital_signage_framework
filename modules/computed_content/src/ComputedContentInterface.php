<?php

namespace Drupal\digital_signage_computed_content;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a digsig_computed_content entity type.
 */
interface ComputedContentInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the digsig_computed_content title.
   *
   * @return string
   *   Title of the digsig_computed_content.
   */
  public function getTitle(): string;

  /**
   * Sets the digsig_computed_content title.
   *
   * @param string $title
   *   The digsig_computed_content title.
   *
   * @return \Drupal\digital_signage_computed_content\ComputedContentInterface
   *   The called digsig_computed_content entity.
   */
  public function setTitle(string $title): ComputedContentInterface;

  /**
   * Gets the digsig_computed_content creation timestamp.
   *
   * @return int
   *   Creation timestamp of the digsig_computed_content.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the digsig_computed_content creation timestamp.
   *
   * @param int $timestamp
   *   The digsig_computed_content creation timestamp.
   *
   * @return \Drupal\digital_signage_computed_content\ComputedContentInterface
   *   The called digsig_computed_content entity.
   */
  public function setCreatedTime(int $timestamp): ComputedContentInterface;

  /**
   * Returns the digsig_computed_content status.
   *
   * @return bool
   *   TRUE if the digsig_computed_content is enabled, FALSE otherwise.
   */
  public function isEnabled(): bool;

  /**
   * Sets the digsig_computed_content status.
   *
   * @param bool $status
   *   TRUE to enable this digsig_computed_content, FALSE to disable.
   *
   * @return \Drupal\digital_signage_computed_content\ComputedContentInterface
   *   The called digsig_computed_content entity.
   */
  public function setStatus(bool $status): ComputedContentInterface;

}
