<?php

namespace Drupal\digital_signage_computed_content;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigImporterEvent;
use Drupal\views\Entity\View;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Config subscriber.
 */
class ConfigSubscriber implements EventSubscriberInterface {

  /**
   * List with imported views.
   *
   * @var array
   */
  private array $importedViews = [];

  /**
   * Remember imported views.
   *
   * @param \Drupal\Core\Config\ConfigImporterEvent $event
   *   Public function onConfigImportValidate event.
   */
  public function onConfigImportValidate(ConfigImporterEvent $event): void {
    foreach ($event->getChangelist() as $items) {
      foreach ($items as $item) {
        if (str_starts_with($item, 'views.view.')) {
          $this->importedViews[] = $item;
        }
      }
    }
  }

  /**
   * Process imported views.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Public function onConfigSave event.
   */
  public function onConfigSave(ConfigCrudEvent $event): void {
    $item = $event->getConfig();
    if (in_array($item->getName(), $this->importedViews, TRUE)) {
      /** @var \Drupal\views\Entity\View $view */
      $view = View::load($item->get('id'));
      foreach ($item->get('display') as $displayId => $def) {
        $view->getExecutable()->setDisplay($displayId);
        $display = $view->getExecutable()->getDisplay();
        $display->validate();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[ConfigEvents::IMPORT_VALIDATE][] = ['onConfigImportValidate', 1024];
    $events[ConfigEvents::SAVE][] = ['onConfigSave', 1024];
    return $events;
  }

}
