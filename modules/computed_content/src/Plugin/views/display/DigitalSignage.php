<?php

namespace Drupal\digital_signage_computed_content\Plugin\views\display;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\digital_signage_computed_content\ComputedContentInterface;
use Drupal\digital_signage_computed_content\Entity\ComputedContent;
use Drupal\digital_signage_framework\Entity\ContentSetting;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The plugin that handles a digital signage content entity.
 *
 * @ingroup views_display_plugins
 *
 * @ViewsDisplay(
 *   id = "digital_signage",
 *   title = @Translation("Digital Signage"),
 *   help = @Translation("Include the view into digital signage."),
 *   theme = "views_view",
 *   uses_menu_links = FALSE
 * )
 */
class DigitalSignage extends DisplayPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a DigitalSignage object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  final public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $errors = parent::validate();
    $this->getComputedContentEntity();
    return $errors;
  }

  /**
   * Loads or creates the entity associated with this view.
   *
   * @return \Drupal\digital_signage_computed_content\ComputedContentInterface
   *   The associated entity.
   */
  protected function getComputedContentEntity(): ComputedContentInterface {
    try {
      $query = $this->entityTypeManager->getStorage('digsig_computed_content')
        ->getQuery()
        ->accessCheck(FALSE);
      $entities = $query
        ->condition('field_view', $this->view->id())
        ->condition('field_display', $this->display['id'])
        ->execute();
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException) {
      // @todo Log this exception.
      $entities = [];
    }
    if (empty($entities)) {
      $settings = ContentSetting::create(['status' => 1]);
      $this->saveEntity($settings);
      /** @var \Drupal\digital_signage_computed_content\ComputedContentInterface $content */
      $content = ComputedContent::create([
        'bundle' => 'view',
        'digital_signage' => $settings,
        'field_view' => $this->view->id(),
        'field_display' => $this->display['id'],
        'title' => $this->getComputedContentTitle(),
      ]);
      $this->saveEntity($content);
    }
    else {
      /** @var \Drupal\digital_signage_computed_content\ComputedContentInterface $content */
      $content = ComputedContent::load(reset($entities));
    }
    return $content;
  }

  /**
   * Safely save the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to save.
   *
   * @return bool
   *   TRUE, if this was successful, FALSE otherwise.
   */
  protected function saveEntity(EntityInterface $entity): bool {
    try {
      $entity->save();
      return TRUE;
    }
    catch (EntityStorageException) {
      // @todo Log this exception.
    }
    return FALSE;
  }

  /**
   * Build the title of the associated entity.
   *
   * @return string
   *   The title.
   */
  protected function getComputedContentTitle(): string {
    return 'View ' . $this->view->getTitle() . ': ' . $this->display['display_title'];
  }

}
