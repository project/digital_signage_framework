<?php

namespace Drupal\digital_signage_computed_content\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\digital_signage_computed_content\ComputedContentInterface;
use Drupal\user\UserInterface;

/**
 * Defines the digsig_computed_content entity class.
 *
 * @ContentEntityType(
 *   id = "digsig_computed_content",
 *   label = @Translation("Computed content"),
 *   label_collection = @Translation("Computed contents"),
 *   bundle_label = @Translation("Computed content type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\digital_signage_computed_content\ComputedContentListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\digital_signage_computed_content\ComputedContentAccessControlHandler",
 *     "form" = {
 *       "edit" = "Drupal\digital_signage_computed_content\Form\ComputedContentForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "digsig_computed_content",
 *   data_table = "digsig_computed_content_field_data",
 *   revision_table = "digsig_computed_content_revision",
 *   revision_data_table = "digsig_computed_content_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer digsig_computed_content types",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "bundle" = "bundle",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "canonical" = "/digsig_computed_content/{digsig_computed_content}",
 *     "edit-form" = "/admin/content/digsig-computed-content/{digsig_computed_content}/edit",
 *     "collection" = "/admin/content/digsig-computed-content"
 *   },
 *   bundle_entity_type = "digsig_computed_content_type",
 *   field_ui_base_route = "entity.digsig_computed_content_type.edit_form"
 * )
 */
class ComputedContent extends RevisionableContentEntityBase implements ComputedContentInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new digsig_computed_content entity is created, set the uid entity
   * reference to the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values): void {
    parent::preCreate($storage, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title): ComputedContentInterface {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): ComputedContentInterface {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp): ComputedContentInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    /** @var \Drupal\user\UserInterface $user */
    $user = $this->get('uid')->entity;
    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): ?int {
    $id = $this->get('uid')->target_id;
    return ($id) ? (int) $id : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): ComputedContentInterface {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): ComputedContentInterface {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the digsig_computed_content entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', FALSE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Status'))
      ->setDescription(t('A boolean indicating whether the digsig_computed_content is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', FALSE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the digsig_computed_content author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the digsig_computed_content was created.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', FALSE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the digsig_computed_content was last edited.'));

    return $fields;
  }

}
