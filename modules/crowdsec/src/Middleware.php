<?php

namespace Drupal\digital_signage_crowdsec;

use Drupal\crowdsec\Middleware as CrowdSecMiddleware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Provides a HTTP middleware to integrate Digital Signage with CrowdSec.
 */
class Middleware implements HttpKernelInterface {

  /**
   * The decorated kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected HttpKernelInterface $httpKernel;

  /**
   * Constructs a Digital Signage CrowdSec Middleware object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   */
  public function __construct(HttpKernelInterface $http_kernel) {
    $this->httpKernel = $http_kernel;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = HttpKernelInterface::MAIN_REQUEST, $catch = TRUE): Response {
    if (str_ends_with($request->getPathInfo(), '/api/digital_signage')) {
      CrowdSecMiddleware::disable();
    }
    return $this->httpKernel->handle($request, $type, $catch);
  }

}
