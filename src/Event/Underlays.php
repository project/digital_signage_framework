<?php

namespace Drupal\digital_signage_framework\Event;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\digital_signage_framework\DeviceInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * The event that gets dispatched when underlays are collected.
 *
 * @package Drupal\digital_signage_framework\Event
 */
class Underlays extends Event {

  /**
   * The underlays.
   *
   * @var array
   */
  protected array $underlays = [];

  /**
   * The libraries.
   *
   * @var array
   */
  protected array $libraries = [];

  /**
   * The device.
   *
   * @var \Drupal\digital_signage_framework\DeviceInterface
   */
  protected DeviceInterface $device;

  /**
   * Rendered constructor.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   */
  public function __construct(DeviceInterface $device) {
    $this->device = $device;
  }

  /**
   * Gets the device.
   *
   * @return \Drupal\digital_signage_framework\DeviceInterface
   *   The device.
   */
  public function getDevice(): DeviceInterface {
    return $this->device;
  }

  /**
   * Gets the underlays.
   *
   * @return array
   *   The underlays.
   */
  public function getUnderlays(): array {
    $result = [];
    foreach ($this->underlays as $underlay) {
      $result[] = $underlay['markup'];
    }
    return $result;
  }

  /**
   * Gets the libraries.
   *
   * @return array
   *   The libraries.
   */
  public function getLibraries(): array {
    return $this->libraries;
  }

  /**
   * Adds a new underlay.
   *
   * @param string $id
   *   The ID of the underlay.
   * @param string|TranslatableMarkup $label
   *   The label.
   * @param string $markup
   *   The content markup.
   * @param array $attached
   *   The attachments.
   *
   * @return self
   *   The underlay event.
   */
  public function addUnderlay(string $id, string|TranslatableMarkup $label, string $markup, array $attached): Underlays {
    $this->underlays[] = [
      'id' => $id,
      'label' => $label,
      'markup' => $markup,
    ];
    if (isset($attached['library'])) {
      foreach ($attached['library'] as $library) {
        $this->libraries[] = $library;
      }
    }
    return $this;
  }

}
