<?php

namespace Drupal\digital_signage_framework\Event;

use Drupal\digital_signage_framework\DeviceInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * The event that gets dispatched when content for a device gets rendered.
 *
 * @package Drupal\digital_signage_framework\Event
 */
class Rendered extends Event {

  /**
   * The response.
   *
   * @var \Symfony\Component\HttpFoundation\Response
   */
  protected Response $response;

  /**
   * The device.
   *
   * @var \Drupal\digital_signage_framework\DeviceInterface
   */
  protected DeviceInterface $device;

  /**
   * Rendered constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response.
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The devices.
   */
  public function __construct(Response $response, DeviceInterface $device) {
    $this->response = $response;
    $this->device = $device;
  }

  /**
   * Gets the response.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function getResponse(): Response {
    return $this->response;
  }

  /**
   * Gets the device.
   *
   * @return \Drupal\digital_signage_framework\DeviceInterface
   *   The device.
   */
  public function getDevice(): DeviceInterface {
    return $this->device;
  }

}
