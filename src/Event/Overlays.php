<?php

namespace Drupal\digital_signage_framework\Event;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\digital_signage_framework\DeviceInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * The event that gets dispatched when overlays are collected.
 *
 * @package Drupal\digital_signage_framework\Event
 */
class Overlays extends Event {

  /**
   * The overlays.
   *
   * @var array
   */
  protected array $overlays = [];

  /**
   * The libraries.
   *
   * @var array
   */
  protected array $libraries = [];

  /**
   * The device.
   *
   * @var \Drupal\digital_signage_framework\DeviceInterface
   */
  protected DeviceInterface $device;

  /**
   * Rendered constructor.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   */
  public function __construct(DeviceInterface $device) {
    $this->device = $device;
  }

  /**
   * Gets the device.
   *
   * @return \Drupal\digital_signage_framework\DeviceInterface
   *   The device.
   */
  public function getDevice(): DeviceInterface {
    return $this->device;
  }

  /**
   * Gets the overlays.
   *
   * @return array
   *   The overlays.
   */
  public function getOverlays(): array {
    $result = [];
    foreach ($this->overlays as $overlay) {
      $result[] = $overlay['markup'];
    }
    return $result;
  }

  /**
   * Gets the libraries.
   *
   * @return array
   *   The libraries.
   */
  public function getLibraries(): array {
    return $this->libraries;
  }

  /**
   * Adds a new overlay.
   *
   * @param string $id
   *   The ID of the overlay.
   * @param string|TranslatableMarkup $label
   *   The label.
   * @param string $markup
   *   The content markup.
   * @param array $attached
   *   The attachments.
   *
   * @return self
   *   The overlay event.
   */
  public function addOverlay(string $id, string|TranslatableMarkup $label, string $markup, array $attached): Overlays {
    $this->overlays[] = [
      'id' => $id,
      'label' => $label,
      'markup' => $markup,
    ];
    if (isset($attached['library'])) {
      foreach ($attached['library'] as $library) {
        $this->libraries[] = $library;
      }
    }
    return $this;
  }

}
