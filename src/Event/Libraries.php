<?php

namespace Drupal\digital_signage_framework\Event;

use Drupal\digital_signage_framework\DeviceInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * The event that gets dispatched when libraries are collected.
 *
 * @package Drupal\digital_signage_framework\Event
 */
class Libraries extends Event {

  /**
   * The list of to be added libraries.
   *
   * @var array
   */
  protected array $libraries = [];

  /**
   * The list of to be added Drupal settings.
   *
   * @var array
   */
  protected array $settings = [];

  /**
   * The device entity.
   *
   * @var \Drupal\digital_signage_framework\DeviceInterface
   */
  protected DeviceInterface $device;

  /**
   * Libraries constructor.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device entity.
   */
  public function __construct(DeviceInterface $device) {
    $this->device = $device;
  }

  /**
   * Gets the to be added libraries.
   *
   * @return array
   *   The to be added libraries.
   */
  public function getLibraries(): array {
    return $this->libraries;
  }

  /**
   * Gets the to be added Drupal settings.
   *
   * @return array
   *   The to be added Drupal settings.
   */
  public function getSettings(): array {
    return $this->settings;
  }

  /**
   * Add a library.
   *
   * @param string $library
   *   The key of the library to be added.
   *
   * @return self
   *   This service.
   */
  public function addLibrary(string $library): Libraries {
    $this->libraries[] = $library;
    return $this;
  }

  /**
   * Add settings.
   *
   * @param string $module
   *   The module name for which settings should be added.
   * @param array $settings
   *   The to be added settings.
   *
   * @return self
   *   This service.
   */
  public function addSettings(string $module, array $settings): Libraries {
    $this->settings[$module] = $settings;
    return $this;
  }

  /**
   * Get the device this service is currently working with.
   *
   * @return \Drupal\digital_signage_framework\DeviceInterface
   *   The device entity.
   */
  public function getDevice(): DeviceInterface {
    return $this->device;
  }

}
