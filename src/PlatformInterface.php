<?php

namespace Drupal\digital_signage_framework;

/**
 * Interface for digital_signage_platform plugins.
 */
interface PlatformInterface {

  /**
   * Initialize the plugin.
   */
  public function init(): void;

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Stores data to the temp store.
   *
   * @param string $id
   *   The ID.
   * @param mixed $value
   *   The data.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function storeRecord(string $id, mixed $value): void;

  /**
   * Deletes data from the temp store.
   *
   * @param string $id
   *   The ID.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function deleteRecord(string $id): void;

  /**
   * Get data from the temp store.
   *
   * @param string $id
   *   The ID.
   *
   * @return mixed
   *   The data.
   */
  public function getRecord(string $id): mixed;

  /**
   * Provide a list of extra base fields required for the platform's schedule.
   *
   * @param array $fields
   *   The field definitions.
   */
  public function scheduleBaseFields(array &$fields): void;

  /**
   * Syncs devices of this platform.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function syncDevices(): void;

  /**
   * Receive a list of all devices from the platform.
   *
   * @return \Drupal\digital_signage_framework\DeviceInterface[]
   *   The list of devices.
   */
  public function getPlatformDevices(): array;

  /**
   * Push the current schedule to a device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   * @param bool $debug
   *   Flag, whether the device should be set into debug mode.
   * @param bool $reload_assets
   *   Flag, whether the device should reload assets.
   * @param bool $reload_content
   *   Flag, whether the device should reload the content.
   */
  public function pushSchedule(DeviceInterface $device, bool $debug, bool $reload_assets, bool $reload_content): void;

  /**
   * Push configuration to a device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   * @param bool $debug
   *   Flag, whether the device should be set into debug mode.
   * @param bool $reload_schedule
   *   Flag, whether the device should reload the schedule.
   * @param bool $reload_assets
   *   Flag, whether the device should reload assets.
   * @param bool $reload_content
   *   Flag, whether the device should reload the content.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function pushConfiguration(DeviceInterface $device, bool $debug, bool $reload_schedule, bool $reload_assets, bool $reload_content): void;

  /**
   * Enable emergency mode on a device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   * @param string $entity_type
   *   The entity type to display in emergency mode.
   * @param int $entity_id
   *   The entity ID to display in emergency mode.
   */
  public function setEmergencyMode(DeviceInterface $device, string $entity_type, int $entity_id): void;

  /**
   * Disable emergency mode on a device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   */
  public function disableEmergencyMode(DeviceInterface $device): void;

  /**
   * Enable debug mode on the device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   */
  public function debugDevice(DeviceInterface $device): void;

  /**
   * Get the debug log from a device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   *
   * @return array
   *   The debug log.
   */
  public function showDebugLog(DeviceInterface $device): array;

  /**
   * Get the error log from a device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   *
   * @return array
   *   The error log.
   */
  public function showErrorLog(DeviceInterface $device): array;

  /**
   * Get the slide display report from a device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   *
   * @return array
   *   The slide display report.
   */
  public function showSlideReport(DeviceInterface $device): array;

  /**
   * Get screenshots from the device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   * @param bool $refresh
   *   Indicator if fresh screenshots should be taken.
   *
   * @return array
   *   Key value pairs with "takenAt" and "uri" as the keys for a date and
   *   the screenshot uri.
   */
  public function getScreenshot(DeviceInterface $device, bool $refresh = FALSE): array;

}
