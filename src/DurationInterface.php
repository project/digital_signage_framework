<?php

namespace Drupal\digital_signage_framework;

/**
 * Interface for duration objects.
 */
interface DurationInterface {

  /**
   * Returns the offset for critical entities.
   *
   * @return int
   *   The offset for critical entities.
   */
  public function getOffsetForCritical(): int;

  /**
   * Returns the offset for the given complexity type.
   *
   * @param string $complexityType
   *   The complexity type.
   *
   * @return int
   *   The offset by complexity.
   */
  public function getOffsetByComplexity(string $complexityType): int;

  /**
   * Returns the duration for the given complexity type.
   *
   * @param string $complexityType
   *   The complexity type.
   *
   * @return int
   *   The duration by complexity.
   */
  public function getDurationByComplexity(string $complexityType): int;

}
