<?php

namespace Drupal\digital_signage_framework;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a schedule entity type.
 */
interface ScheduleInterface extends ContentEntityInterface {

  /**
   * Gets the schedule creation timestamp.
   *
   * @return int
   *   Creation timestamp of the schedule.
   */
  public function getCreatedTime(): int;

  /**
   * Get the list of items in this schedule.
   *
   * @return array
   *   The list of items.
   */
  public function getItems(): array;

  /**
   * Optionally set and get whether this schedules needs to be pushed.
   *
   * @param bool|null $flag
   *   Provide TRUE or FALSE to set the status, leave to NULL otherwise.
   *
   * @return bool
   *   The current status.
   */
  public function needsPush(?bool $flag = NULL): bool;

}
