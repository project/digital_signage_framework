<?php

namespace Drupal\digital_signage_framework\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ActiveTheme;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\block\BlockRepositoryInterface;
use Drupal\block\BlockViewBuilder;
use Drupal\digital_signage_framework\DigitalSignageFrameworkEvents;
use Drupal\digital_signage_framework\Event\Overlays;
use Drupal\digital_signage_framework\Event\Underlays;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Digital signage event subscriber for blocks.
 */
class Blocks implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The core renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The block repository.
   *
   * @var \Drupal\block\BlockRepositoryInterface
   */
  protected BlockRepositoryInterface $blockRepository;

  /**
   * The block view builder.
   *
   * @var \Drupal\block\BlockViewBuilder
   */
  protected BlockViewBuilder $blockViewBuilder;

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected ThemeManagerInterface $themeManager;

  /**
   * The current theme.
   *
   * @var \Drupal\Core\Theme\ActiveTheme
   */
  protected ActiveTheme $currentTheme;

  /**
   * The default theme.
   *
   * @var \Drupal\Core\Theme\ActiveTheme
   */
  protected ActiveTheme $defaultTheme;

  /**
   * Blocks constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The core renderer.
   * @param \Drupal\block\BlockRepositoryInterface $blockRepository
   *   The block repository.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   The theme manager.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The theme handler.
   * @param \Drupal\Core\Theme\ThemeInitializationInterface $themeInitialization
   *   The theme initialization service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RendererInterface $renderer, BlockRepositoryInterface $blockRepository, ThemeManagerInterface $themeManager, ThemeHandlerInterface $themeHandler, ThemeInitializationInterface $themeInitialization) {
    $this->renderer = $renderer;
    $this->blockRepository = $blockRepository;
    $builder = $entityTypeManager->getViewBuilder('block');
    if ($builder instanceof BlockViewBuilder) {
      $this->blockViewBuilder = $builder;
    }
    $this->themeManager = $themeManager;
    $this->currentTheme = $this->themeManager->getActiveTheme();
    $this->defaultTheme = $themeInitialization->initTheme($themeHandler->getDefault());
  }

  /**
   * Renders blocks of a region.
   *
   * @param string $region
   *   The region.
   *
   * @return array
   *   The render array.
   */
  protected function renderBlocks(string $region): array {
    $this->themeManager->setActiveTheme($this->defaultTheme);

    $content = [];
    $cacheable_metadata_list = [];
    foreach ($this->blockRepository->getVisibleBlocksPerRegion($cacheable_metadata_list) as $theme_region => $blocks) {
      if (!empty($blocks) && $theme_region === $region) {
        /** @var \Drupal\block\Entity\Block $block */
        foreach ($blocks as $block) {
          $build = $this->blockViewBuilder->view($block);
          $content[] = [
            'id' => $block->id(),
            'label' => $block->label(),
            'content' => $this->renderer->renderPlain($build),
            'attached' => $build['#attached'] ?? [],
          ];
        }
      }
    }
    $this->themeManager->setActiveTheme($this->currentTheme);

    return $content;
  }

  /**
   * Dispatched when underlays get collected.
   *
   * @param \Drupal\digital_signage_framework\Event\Underlays $event
   *   The underlay event.
   */
  public function onUnderlays(Underlays $event): void {
    foreach ($this->renderBlocks('digital_signage_underlays') as $renderBlock) {
      $event->addUnderlay($renderBlock['id'], $renderBlock['label'], $renderBlock['content'], $renderBlock['attached']);
    }
  }

  /**
   * Dispatched when overlays get collected.
   *
   * @param \Drupal\digital_signage_framework\Event\Overlays $event
   *   The overlay event.
   */
  public function onOverlays(Overlays $event): void {
    foreach ($this->renderBlocks('digital_signage_overlays') as $renderBlock) {
      $event->addOverlay($renderBlock['id'], $renderBlock['label'], $renderBlock['content'], $renderBlock['attached']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      DigitalSignageFrameworkEvents::OVERLAYS => ['onOverlays'],
      DigitalSignageFrameworkEvents::UNDERLAYS => ['onUnderlays'],
    ];
  }

}
