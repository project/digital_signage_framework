<?php

namespace Drupal\digital_signage_framework;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\media\Entity\MediaType;

/**
 * Object representing an item in a schedule.
 */
class SequenceItem {

  /**
   * The item ID.
   *
   * @var int
   */
  protected int $id;

  /**
   * The item type.
   *
   * @var string
   */
  protected string $type;

  /**
   * The ID of the entity.
   *
   * @var int
   */
  protected int $entityId;

  /**
   * The type of the entity.
   *
   * @var string
   */
  protected string $entityType;

  /**
   * The bundle of the entity.
   *
   * @var string
   */
  protected string $entityBundle;

  /**
   * The optional label of the item.
   *
   * @var string
   */
  protected string $entityLabel;

  /**
   * The display duration of the item.
   *
   * @var int
   */
  protected int $duration;

  /**
   * Indicator, if the item is dynamic content.
   *
   * @var bool
   */
  protected bool $isDynamic;

  /**
   * The list of successor items.
   *
   * @var array|null
   */

  protected ?array $successors = NULL;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  protected ?EntityTypeManagerInterface $entityTypeManager = NULL;

  /**
   * SequenceItem constructor.
   *
   * @param int $id
   *   The item ID.
   * @param int $contentEntityId
   *   The ID of the entity.
   * @param string $contentEntityType
   *   The type of the entity.
   * @param string $contentEntityBundle
   *   The bundle of the entity.
   * @param string|null $contentEntityLabel
   *   The optional label of the item.
   * @param int $duration
   *   The display duration of the item.
   * @param bool $isDynamic
   *   Indicator, if the item is dynamic content.
   */
  public function __construct(int $id, int $contentEntityId, string $contentEntityType, string $contentEntityBundle, ?string $contentEntityLabel, int $duration, bool $isDynamic) {
    $this->id = $id;
    $this->entityId = $contentEntityId;
    $this->entityType = $contentEntityType;
    $this->entityBundle = $contentEntityBundle;
    $this->entityLabel = $contentEntityLabel ?? implode(' ', [$contentEntityType, $contentEntityId]);
    $this->duration = $duration;
    $this->isDynamic = $isDynamic;
    $this->determineType();
  }

  /**
   * Returns a sequence item by the given entity and duration.
   *
   * @param \Drupal\digital_signage_framework\ContentSettingInterface $contentSetting
   *   The settings entity.
   * @param int $duration
   *   The display duration.
   *
   * @return self
   *   This sequence item.
   */
  public static function create(ContentSettingInterface $contentSetting, int $duration) : SequenceItem {
    return new SequenceItem(
      $contentSetting->id(),
      $contentSetting->getReverseEntityId(),
      $contentSetting->getReverseEntityType(),
      $contentSetting->getReverseEntityBundle(),
      $contentSetting->label(),
      $duration,
      $contentSetting->isDynamic()
    );
  }

  /**
   * Gets the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  protected function entityTypeManager(): EntityTypeManagerInterface {
    if ($this->entityTypeManager === NULL) {
      // @phpstan-ignore-next-line
      $this->entityTypeManager = \Drupal::entityTypeManager();
    }
    return $this->entityTypeManager;
  }

  /**
   * Gets the list of successors for this item on a device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   *
   * @return array
   *   The list of successors for this item.
   */
  public function getSuccessors(DeviceInterface $device): array {
    if ($this->successors === NULL) {
      $this->successors = [];
      $entityTypeManager = $this->entityTypeManager();
      try {
        $query = $entityTypeManager
          ->getStorage('digital_signage_content_setting')
          ->getQuery()
          ->accessCheck(FALSE)
          ->condition('status', 1)
          ->condition('emergencymode', 0);
        $orQuery = $query->orConditionGroup()
          ->condition($query->andConditionGroup()
            ->notExists('devices.target_id')
            ->notExists('segments.target_id')
          )
          ->condition('devices.target_id', $device->id());
        if ($segmentIds = $device->getSegmentIds()) {
          $orQuery->condition('segments.target_id', $segmentIds, 'IN');
        }
        $ids = $query
          ->condition($orQuery)
          ->condition('predecessor', $this->id)
          ->execute();
        /** @var int[] $ids */
        array_walk($ids, static function (&$item) {
          $item = (int) $item;
        });
        /** @var \Drupal\digital_signage_framework\ContentSettingInterface $item */
        foreach ($entityTypeManager->getStorage('digital_signage_content_setting')->loadMultiple($ids) as $item) {
          $this->successors[] = self::create($item, $this->duration);
        }
      }
      catch (InvalidPluginDefinitionException | PluginNotFoundException) {
        // Can be ignored.
      }
    }
    return $this->successors;
  }

  /**
   * Determine content type.
   */
  protected function determineType(): void {
    $this->type = 'html';
    if ($this->entityType === 'media') {
      /** @var \Drupal\media\MediaTypeInterface $mediaType */
      $mediaType = MediaType::load($this->entityBundle);
      if (($fieldDefinition = $mediaType->getSource()->getSourceFieldDefinition($mediaType)) &&
          $fieldDefinition->getSetting('handler')) {
        if ($mediaType->getSource()->getPluginId() === 'file' && str_contains($mediaType->id(), 'video')) {
          $this->type = 'video';
        }
        elseif (!str_contains($mediaType->getSource()->getPluginId(), 'video')) {
          if ($mediaType->getSource()->getPluginId() !== 'svg') {
            $this->type = 'image';
          }
        }
        else {
          $this->type = 'video';
        }
      }
    }
  }

  /**
   * Gets the item ID.
   *
   * @return int
   *   The item ID.
   */
  public function id(): int {
    return $this->id;
  }

  /**
   * Gets the list of properties.
   *
   * @return array
   *   The list of properties.
   */
  public function toArray(): array {
    return [
      'type' => $this->getType(),
      'entity' => [
        'type' => $this->getEntityType(),
        'id' => $this->getEntityId(),
      ],
      'label' => $this->getEntityLabel(),
      'duration' => $this->getDuration(),
      'dynamic' => $this->getIsDynamic(),
    ];
  }

  /**
   * Gets the item type.
   *
   * @return string|null
   *   The item type.
   */
  public function getType(): ?string {
    return $this->type;
  }

  /**
   * Returns the content entity ID.
   *
   * @return int
   *   The entity ID.
   */
  public function getEntityId(): int {
    return $this->entityId;
  }

  /**
   * Returns the content entity type ID.
   *
   * @return string
   *   The entity type.
   */
  public function getEntityType(): string {
    return $this->entityType;
  }

  /**
   * Returns the content entity label.
   *
   * @return string
   *   The entity label.
   */
  public function getEntityLabel(): string {
    return $this->entityLabel;
  }

  /**
   * Returns the duration in seconds to display.
   *
   * @return int
   *   The display duration.
   */
  public function getDuration(): int {
    return $this->duration;
  }

  /**
   * Returns TRUE if the content is dynamic or FALSE otherwise.
   *
   * @return bool
   *   TRUE, if this item is dynamic content, FALSE otherwise.
   */
  public function getIsDynamic(): bool {
    return $this->isDynamic;
  }

}
