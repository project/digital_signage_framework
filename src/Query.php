<?php

namespace Drupal\digital_signage_framework;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Service provides commonly used database queries.
 */
class Query {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The emregency service.
   *
   * @var \Drupal\digital_signage_framework\Emergency
   */
  protected Emergency $emergency;

  /**
   * Renderer constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\digital_signage_framework\Emergency $emergency
   *   The emergency service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Emergency $emergency) {
    $this->entityTypeManager = $entity_type_manager;
    $this->emergency = $emergency;
  }

  /**
   * Gets a list of all entities contained in any schedule for a form select.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface[] $devices
   *   The list of devices.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   The list of entities.
   */
  public function allEntitiesForSelect(array $devices): array {
    $entities = [];
    foreach ($devices as $device) {
      if ($schedule = $device->getSchedule(FALSE)) {
        foreach ($schedule->getItems() as $item) {
          $key = implode('/', [$item['entity']['type'], $item['entity']['id']]);
          if (!isset($entities[$key])) {
            try {
              if ($entity = $this->entityTypeManager->getStorage($item['entity']['type'])
                ->load($item['entity']['id'])) {
                $entities[$key] = $entity->label();
              }
            }
            catch (InvalidPluginDefinitionException | PluginNotFoundException) {
              // Can be ignored.
            }
          }
        }
      }
    }
    foreach ($this->emergency->allForSelect() as $key => $label) {
      $entities[$key] = $label;
    }
    return $entities;
  }

}
