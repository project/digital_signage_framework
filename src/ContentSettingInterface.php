<?php

namespace Drupal\digital_signage_framework;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a digital signage content setting entity type.
 */
interface ContentSettingInterface extends ContentEntityInterface {

  /**
   * Determines if the settings got changed.
   *
   * @return bool
   *   TRUE, if the settings got changed, FALSE otherwise.
   */
  public function hasChanged(): bool;

  /**
   * Get the parent entity.
   *
   * @return array|null
   *   The parent entity, if that exists, NULL otherwise.
   */
  public function getReverseEntity(): ?array;

  /**
   * Sets the parent entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The parent entity.
   *
   * @return self
   *   This content setting object.
   */
  public function setReverseEntity(ContentEntityInterface $entity): ContentSettingInterface;

  /**
   * Get the type of the parent entity.
   *
   * @return string|null
   *   The type of the parent entity, if that exists, NULL otherwise.
   */
  public function getReverseEntityType(): ?string;

  /**
   * Get the bundle of the parent entity.
   *
   * @return string|null
   *   The bundle of the parent entity, if that exists, NULL otherwise.
   */
  public function getReverseEntityBundle(): ?string;

  /**
   * Get the ID of the parent entity.
   *
   * @return int
   *   The ID of the parent entity, if that exists, 0 otherwise.
   */
  public function getReverseEntityId(): int;

  /**
   * Returns the digital signage content setting status.
   *
   * @return bool
   *   TRUE if the digital signage content setting is enabled, FALSE otherwise.
   */
  public function isReverseEntityEnabled(): bool;

  /**
   * Sets the digital signage content setting status.
   *
   * @param bool $status
   *   TRUE to enable this digital signage content setting, FALSE to disable.
   *
   * @return self
   *   This content setting object.
   */
  public function setReverseEntityStatus(bool $status): ContentSettingInterface;

  /**
   * Returns the digital signage content setting status.
   *
   * @return bool
   *   TRUE if the digital signage content setting is enabled, FALSE otherwise.
   */
  public function isEnabled(): bool;

  /**
   * Sets the digital signage content setting status.
   *
   * @param bool $status
   *   TRUE to enable this digital signage content setting, FALSE to disable.
   *
   * @return self
   *   This content setting object.
   */
  public function setStatus(bool $status): ContentSettingInterface;

  /**
   * Get the list of devices IDs this content setting is assigned to.
   *
   * @return int[]
   *   The list of device IDs.
   */
  public function getDeviceIds(): array;

  /**
   * Get the list of segment IDs this content setting is assigned to.
   *
   * @return int[]
   *   The list of segment IDs.
   */
  public function getSegmentIds(): array;

  /**
   * Returns the priority.
   *
   * @return int
   *   The priority.
   */
  public function getPriority(): int;

  /**
   * Returns the type of complexity.
   *
   * @return string
   *   The type of complexity.
   */
  public function getType(): string;

  /**
   * Returns whether the entity is critical.
   *
   * @return bool
   *   TRUE, if the entity is critical, FALSE otherwise.
   */
  public function isCritical(): bool;

  /**
   * Returns whether the entity is dynamic content.
   *
   * @return bool
   *   TRUE, if the entity is dynamic content, FALSE otherwise.
   */
  public function isDynamic(): bool;

  /**
   * Sets the digital signage content setting dynamic content flag.
   *
   * @param bool $flag
   *   TRUE to declare this digital signage content setting dynamic, FALSE
   *   otherwise.
   *
   * @return self
   *   This content setting object.
   */
  public function setDynamic(bool $flag): ContentSettingInterface;

  /**
   * Returns the auto-label mode.
   *
   * @return bool
   *   TRUE if auto-label is enabled, FALSE otherwise.
   */
  public function isAutoLabel(): bool;

  /**
   * Returns the label.
   *
   * @return string
   *   The label.
   */
  public function getLabel(): string;

  /**
   * Sets the label.
   *
   * @param string $label
   *   The label.
   *
   * @return self
   *   This content setting object.
   */
  public function setLabel(string $label): ContentSettingInterface;

}
