<?php

namespace Drupal\digital_signage_framework;

use Drupal\Core\Config\ImmutableConfig;

/**
 * Object representing the default weight.
 */
class DefaultWeight implements WeightInterface {

  /**
   * The settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $settings;

  /**
   * DefaultDuration constructor.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   *   The settings.
   */
  public function __construct(ImmutableConfig $settings) {
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeightByPriority($priority): int {
    return match ($priority) {
      1 => $this->settings->get('schedule.priority_weight.high'),
      2 => $this->settings->get('schedule.priority_weight.normal'),
      default => $this->settings->get('schedule.priority_weight.low'),
    };
  }

}
