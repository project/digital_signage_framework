<?php

namespace Drupal\digital_signage_framework;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\AttachmentsInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Render\HtmlResponseAttachmentsProcessor;
use Drupal\Core\Render\RendererInterface;

/**
 * The render service.
 */
class Renderer {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The core renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The html response attachment processor.
   *
   * @var \Drupal\Core\Render\HtmlResponseAttachmentsProcessor
   */
  protected HtmlResponseAttachmentsProcessor $attachmentProcessor;

  /**
   * Renderer constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The core renderer.
   * @param \Drupal\Core\Render\HtmlResponseAttachmentsProcessor $attachment_processor
   *   The html response attachment processor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer, HtmlResponseAttachmentsProcessor $attachment_processor) {
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->attachmentProcessor = $attachment_processor;
  }

  /**
   * Builds the render array for an entity view.
   *
   * @param string $entityType
   *   The entity type ID.
   * @param string $entityId
   *   The entity ID.
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   *
   * @return array
   *   The render array.
   */
  public function buildEntityView(string $entityType, string $entityId, DeviceInterface $device): array {
    try {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $this->entityTypeManager->getStorage($entityType)
        ->load($entityId);
      // We know the entity can be loaded because we checked the validity in the
      // access callback.
      /* @noinspection NullPointerExceptionInspection */
      $build = $this->entityTypeManager->getViewBuilder($entityType)
        ->view($entity, 'digital_signage_' . $device->getOrientation());
      $build['#cache'] = ['max-age' => 0];
      return $build;
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException) {
    }
    return [];
  }

  /**
   * Builds the html response for a given output.
   *
   * @param array $output
   *   The output.
   *
   * @return \Drupal\Core\Render\AttachmentsInterface
   *   The html response.
   */
  public function buildHtmlResponse(array $output): AttachmentsInterface {
    $response = new HtmlResponse();
    $response->setContent([
      '#markup' => $this->renderer->renderRoot($output),
      '#attached' => [
        'html_response_attachment_placeholders' => [],
        'placeholders' => [],
        'drupalSettings' => [],
        'library' => [
          'digital_signage/general',
        ],
      ],
    ]);
    return $this->attachmentProcessor->processAttachments($response);
  }

  /**
   * Renders plain output for render array.
   *
   * @param array $elements
   *   The render array.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The plain output.
   */
  public function renderPlain(array &$elements): MarkupInterface {
    return $this->renderer->renderPlain($elements);
  }

}
