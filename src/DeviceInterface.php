<?php

namespace Drupal\digital_signage_framework;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a device entity type.
 */
interface DeviceInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Get the corresponding plugin for the device.
   *
   * @return \Drupal\digital_signage_framework\PlatformInterface
   *   The platform plugin.
   */
  public function getPlugin(): PlatformInterface;

  /**
   * Get the data for the device.
   *
   * @param bool $debug
   *   Indicator, if the device should be set to debug mode.
   * @param bool $reload_assets
   *   Indicator, if the device should reload assets.
   * @param bool $reload_content
   *   Indicator, if the device should reload content.
   *
   * @return array
   *   The data for the device.
   */
  public function getApiSpec(bool $debug = FALSE, bool $reload_assets = FALSE, bool $reload_content = FALSE): array;

  /**
   * Gets the device external ID.
   *
   * @return string
   *   External ID of the device.
   */
  public function extId(): string;

  /**
   * Gets the device title.
   *
   * @return string
   *   Title of the device.
   */
  public function getTitle(): string;

  /**
   * Sets the device title.
   *
   * @param string $title
   *   The device title.
   *
   * @return \Drupal\digital_signage_framework\DeviceInterface
   *   The called device entity.
   */
  public function setTitle(string $title): DeviceInterface;

  /**
   * Gets the device creation timestamp.
   *
   * @return int
   *   Creation timestamp of the device.
   */
  public function getCreatedTime(): int;

  /**
   * Returns the device status.
   *
   * @return bool
   *   TRUE if the device is enabled, FALSE otherwise.
   */
  public function isEnabled(): bool;

  /**
   * Sets the device status.
   *
   * @param bool $status
   *   TRUE to enable this device, FALSE to disable.
   *
   * @return \Drupal\digital_signage_framework\DeviceInterface
   *   The called device entity.
   */
  public function setStatus(bool $status): DeviceInterface;

  /**
   * Add a segment to the device.
   *
   * @param string $segment
   *   The segment.
   *
   * @return bool
   *   TRUE if a new segment got added to the device entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addSegment(string $segment): bool;

  /**
   * Get the segment IDs.
   *
   * @return int[]
   *   The segment IDs.
   */
  public function getSegmentIds(): array;

  /**
   * Returns TRUE if the device needs a schedule update, FALSE otherwise.
   *
   * @return bool
   *   TRUE if the schedule needs to be updated.
   */
  public function needsScheduleUpdate(): bool;

  /**
   * Schedule update for this devices's schedule and save the device.
   *
   * @return \Drupal\digital_signage_framework\DeviceInterface
   *   The called device entity.
   */
  public function scheduleUpdate(): DeviceInterface;

  /**
   * Update for this devices's schedule completed.
   *
   * @return \Drupal\digital_signage_framework\DeviceInterface
   *   The called device entity.
   */
  public function scheduleUpdateCompleted(): DeviceInterface;

  /**
   * Gets the active schedule for this device.
   *
   * @param bool $stored
   *   Whether to receive the stored schedule or a temporary one.
   *
   * @return \Drupal\digital_signage_framework\ScheduleInterface|null
   *   The schedule.
   */
  public function getSchedule(bool $stored = TRUE): ?ScheduleInterface;

  /**
   * Sets the active schedule for this device.
   *
   * @param \Drupal\digital_signage_framework\ScheduleInterface $schedule
   *   The schedule.
   *
   * @return \Drupal\digital_signage_framework\DeviceInterface
   *   The called device entity.
   */
  public function setSchedule(ScheduleInterface $schedule): DeviceInterface;

  /**
   * Gets the resolution width.
   *
   * @return int
   *   The width.
   */
  public function getWidth(): int;

  /**
   * Gets the resolution height.
   *
   * @return int
   *   The height.
   */
  public function getHeight(): int;

  /**
   * Get portrait or landscape as orientation string.
   *
   * @return string
   *   the orientation.
   */
  public function getOrientation(): string;

  /**
   * Get the parent entity.
   *
   * @return ContentSettingInterface|null
   *   The parent entity.
   */
  public function getEmergencyEntity(): ?ContentSettingInterface;

}
