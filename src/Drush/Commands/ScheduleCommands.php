<?php

namespace Drupal\digital_signage_framework\Drush\Commands;

use Drupal\digital_signage_framework\ScheduleManager;
use Drush\Attributes\Command;
use Drush\Attributes\Option;
use Drush\Attributes\Usage;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush command file for digital signage devices.
 */
class ScheduleCommands extends DrushCommands {

  /**
   * The schedule manager.
   *
   * @var \Drupal\digital_signage_framework\ScheduleManager
   */
  protected ScheduleManager $scheduleManager;

  /**
   * DigitalSignageScheduleCommands constructor.
   *
   * @param \Drupal\digital_signage_framework\ScheduleManager $schedule_manager
   *   The schedule manager.
   */
  public function __construct(ScheduleManager $schedule_manager) {
    $this->scheduleManager = $schedule_manager;
    parent::__construct();
  }

  /**
   * Return an instance of these Drush commands.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   *
   * @return \Drupal\digital_signage_framework\Drush\Commands\ScheduleCommands
   *   The instance of Drush commands.
   */
  public static function create(ContainerInterface $container): ScheduleCommands {
    return new ScheduleCommands(
      $container->get('schedule.manager.digital_signage_platform'),
    );
  }

  /**
   * Command to push schedules and configuration to devices.
   */
  #[Command(name: 'digital-signage-schedule:push', aliases: ['dssp'])]
  #[Option(name: 'device', description: 'The ID of the device to which the schedule should be pushed.')]
  #[Option(name: 'force', description: 'Force push the schedule, even if it has not been changed.')]
  #[Option(name: 'debugmode', description: 'Turn on debug mode.')]
  #[Option(name: 'reloadassets', description: 'Configure the device to always reload assets.')]
  #[Option(name: 'reloadcontent', description: 'Configure the device to always reload content.')]
  #[Option(name: 'entitytype', description: 'The entity type, if only one entity should be used as the schedule.')]
  #[Option(name: 'entityid', description: 'The entity id, if only one entity should be used as the schedule.')]
  #[Usage(name: 'digital-signage-schedule:push', description: 'Pushes schedules and configuration to devices.')]
  public function pushSchedules(
    array $options = [
      'device' => NULL,
      'force' => FALSE,
      'debugmode' => FALSE,
      'reloadassets' => FALSE,
      'reloadcontent' => FALSE,
      'entitytype' => NULL,
      'entityid' => NULL,
    ],
  ): void {
    if ($options['entitytype'] !== NULL && $options['entityid'] === NULL) {
      $this->io()->error('Entity ID is required if entity type is given.');
      return;
    }
    $this->scheduleManager->pushSchedules($options['device'], $options['force'], $options['debugmode'], $options['reloadassets'], $options['reloadcontent'], $options['entitytype'], $options['entityid']);
  }

  /**
   * Command to temporarily push updated configuration to devices.
   */
  #[Command(name: 'digital-signage-schedule:config', aliases: ['dssc'])]
  #[Option(name: 'device', description: 'The ID of the device to which the schedule should be pushed.')]
  #[Option(name: 'debugmode', description: 'Turn on debug mode.')]
  #[Option(name: 'reloadschedule', description: 'Configure the device to reload the schedule.')]
  #[Option(name: 'reloadassets', description: 'Configure the device to always reload assets.')]
  #[Option(name: 'reloadcontent', description: 'Configure the device to always reload content.')]
  #[Usage(name: 'digital-signage-schedule:config', description: 'Pushes updated configuration to devices.')]
  public function pushConfiguration(
    array $options = [
      'device' => NULL,
      'debugmode' => FALSE,
      'reloadschedule' => FALSE,
      'reloadassets' => FALSE,
      'reloadcontent' => FALSE,
    ],
  ): void {
    $this->scheduleManager->pushConfiguration($options['device'], $options['debugmode'], $options['reloadschedule'], $options['reloadassets'], $options['reloadcontent']);
  }

}
