<?php

namespace Drupal\digital_signage_framework\Drush\Commands;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\State\StateInterface;
use Drupal\digital_signage_framework\Entity\Device;
use Drush\Attributes\Command;
use Drush\Attributes\Usage;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush command file for digital signage framework.
 */
class FrameworkCommands extends DrushCommands {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * The Drupal logger.
   *
   * Important: we can not use $logger here, since Drush 13 decided to use that
   * for its own purposes and gave it another type.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $drupalLogger;

  /**
   * Constructs the FrameworkCommands.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   */
  public function __construct(StateInterface $state, LoggerChannelInterface $logger) {
    parent::__construct();
    $this->state = $state;
    $this->drupalLogger = $logger;
  }

  /**
   * Return an instance of these Drush commands.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   *
   * @return \Drupal\digital_signage_framework\Drush\Commands\FrameworkCommands
   *   The instance of Drush commands.
   */
  public static function create(ContainerInterface $container): FrameworkCommands {
    return new FrameworkCommands(
      $container->get('state'),
      $container->get('digital_signage_platform.logger')
    );
  }

  /**
   * Enable monitoring.
   */
  #[Command(name: 'digital-signage-framework:monitoring:enable', aliases: ['dsdme'])]
  #[Usage(name: 'digital-signage-framework:monitoring:enable', description: 'Enable monitoring.')]
  public function enableMonitoring(): void {
    $this->state->set('digital-signage-framework:monitoring:status', 'on');
  }

  /**
   * Disable monitoring.
   */
  #[Command(name: 'digital-signage-framework:monitoring:disable', aliases: ['dsdmd'])]
  #[Usage(name: 'digital-signage-framework:monitoring:disable', description: 'Disable monitoring.')]
  public function disableMonitoring(): void {
    $this->state->set('digital-signage-framework:monitoring:status', 'off');
  }

  /**
   * Monitor slide change.
   */
  #[Command(name: 'digital-signage-framework:monitoring:slidechange', aliases: ['dsdmsc'])]
  #[Usage(name: 'digital-signage-framework:monitoring:slidechange', description: 'Monitor slidechange.')]
  public function monitorSlideChange(): void {
    $debug = $this->io()->isVerbose();
    if ($this->state->get('digital-signage-framework:monitoring:status', 'on') !== 'on') {
      if ($debug) {
        $this->io()->writeln('Monitoring is disabled.');
      }
      return;
    }
    foreach (Device::loadMultiple() as $device) {
      if ($debug) {
        $this->io()->writeln('Device: ' . $device->label());
      }
      if (!$device->isEnabled()) {
        if ($debug) {
          $this->io()->writeln('...disabled.');
        }
        continue;
      }
      $state = $this->state->get('digital-signage-device:' . $device->id() . ':status:slide', 'ok');
      $newState = 'ok';
      $schedule = $device->getSchedule();
      if ($schedule && count($schedule->getItems()) > 1) {
        $rows = $device->getPlugin()->showSlideReport($device);
        if (empty($rows)) {
          $newState = 'empty';
        }
      }
      if ($debug) {
        $this->io()->writeln('... ' . $state . '/' . $newState);
      }
      if ($newState !== $state || $newState === 'empty') {
        if ($newState === 'ok') {
          $this->drupalLogger->info('Slide change on device %name restored.', ['%name' => $device->label()]);
        }
        else {
          $this->drupalLogger->critical('Slide change on device %name broken.', ['%name' => $device->label()]);
        }
        $this->state->set('digital-signage-device:' . $device->id() . ':status:slide', $newState);
      }
    }
  }

}
