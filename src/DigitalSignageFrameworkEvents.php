<?php

namespace Drupal\digital_signage_framework;

/**
 * Contains all events thrown by the DigitalSignageFramework module.
 */
final class DigitalSignageFrameworkEvents {

  public const LIBRARIES = 'digital_signage_framework:libraries';

  public const RENDERED = 'digital_signage_framework:rendered';

  public const UNDERLAYS = 'digital_signage_framework:underlay';

  public const OVERLAYS = 'digital_signage_framework:overlay';

}
