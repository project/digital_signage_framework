<?php

namespace Drupal\digital_signage_framework;

/**
 * Interface for digital_signage_schedule_generator plugins.
 */
interface ScheduleGeneratorInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Generates the actual schedule.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   * @param \Drupal\digital_signage_framework\ContentSettingInterface[] $contentSettings
   *   The settings.
   *
   * @return \Drupal\digital_signage_framework\SequenceItem[]
   *   The list of schedule items.
   */
  public function generate(DeviceInterface $device, array $contentSettings): array;

}
