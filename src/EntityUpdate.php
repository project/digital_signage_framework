<?php

namespace Drupal\digital_signage_framework;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Entity\EntityViewMode;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\digital_signage_framework\Entity\ContentSetting;

/**
 * Entity update service.
 */
class EntityUpdate {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected EntityDefinitionUpdateManagerInterface $updateManager;

  /**
   * The entity field update service.
   *
   * @var \Drupal\digital_signage_framework\EntityFieldUpdate
   */
  protected EntityFieldUpdate $entityFieldUpdate;

  /**
   * The entity type and bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * The entity types service.
   *
   * @var \Drupal\digital_signage_framework\EntityTypes
   */
  protected EntityTypes $entityTypesService;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Constructs an Entity update service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $update_manager
   *   The entity definition update manager.
   * @param \Drupal\digital_signage_framework\EntityFieldUpdate $entity_field_update
   *   The entity field update manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity bundle manager.
   * @param \Drupal\digital_signage_framework\EntityTypes $entity_types_service
   *   The entity type service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Drupal container module handler.
   */
  public function __construct(EntityTypeManager $entity_type_manager, EntityDefinitionUpdateManagerInterface $update_manager, EntityFieldUpdate $entity_field_update, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityTypes $entity_types_service, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->updateManager = $update_manager;
    $this->entityFieldUpdate = $entity_field_update;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityTypesService = $entity_types_service;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Collects extra fields for all bundles.
   *
   * @return array
   *   The list of extra fields per bundle.
   */
  public function addExtraFields(): array {
    $extra = [];
    $bundles = $this->entityTypeBundleInfo->getAllBundleInfo();
    /** @var \Drupal\Core\Entity\EntityTypeInterface $definition */
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if (($definition instanceof ContentEntityTypeInterface) &&
        isset($bundles[$definition->id()]) &&
        ($definition->id() !== 'digital_signage_content_setting') &&
        !in_array($definition->id(), $this->entityTypesService->allDisabledIds(), TRUE)) {
        foreach ($bundles[$definition->id()] as $bundle => $def) {
          $extra[$definition->id()][$bundle]['display']['digital_signage_label'] = [
            'label' => t('Digital Signage Label'),
            'weight' => 0,
            'visible' => FALSE,
          ];
          $extra[$definition->id()][$bundle]['display']['digital_signage_label_fit'] = [
            'label' => t('Digital Signage Label (fit text)'),
            'weight' => 0,
            'visible' => FALSE,
          ];
          if ($this->moduleHandler->moduleExists('endroid_qr_code')) {
            $extra[$definition->id()][$bundle]['display']['digital_signage_qr_self'] = [
              'label' => t('QR Code'),
              'weight' => 0,
              'visible' => FALSE,
            ];
          }
        }
      }
    }
    return $extra;
  }

  /**
   * Default definition for the entity reference field.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   The default field definition.
   */
  public function fieldDefinition(): BaseFieldDefinition {
    return BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Digital signage'))
      ->setSetting('target_type', 'digital_signage_content_setting')
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_simple',
        'settings' => [
          'form_mode' => 'default',
          'label_singular' => '',
          'label_plural' => '',
          'collapsible' => TRUE,
          'collapsed' => TRUE,
          'override_labels' => FALSE,

        ],
        'weight' => 99,
      ])
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);
  }

  /**
   * Method description.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateExistingEntityTypes(): void {
    $field_definition = $this->fieldDefinition();
    /** @var \Drupal\Core\Entity\EntityTypeInterface $definition */
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if ($definition instanceof ContentEntityTypeInterface) {
        if ($definition->id() === 'digital_signage_content_setting') {
          $this->entityFieldUpdate->updateFields($definition->id(), ContentSetting::baseFieldDefinitions($definition));
          $this->ensureDisplayModes($definition);
        }
        elseif (!in_array($definition->id(), $this->entityTypesService->allDisabledIds(), TRUE)) {
          $this->updateManager->installFieldStorageDefinition('digital_signage', $definition->id(), $definition->getProvider(), $field_definition);
        }
      }
    }
  }

  /**
   * Determines, if a display mode for a given entity type is available.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $machine_name
   *   The machine name of the display mode to check.
   *
   * @return bool
   *   TRUE, if that display mode exists, FALSE otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function existsDisplayMode(string $entity_type_id, string $machine_name): bool {
    return (bool) $this->entityTypeManager
      ->getStorage('entity_view_mode')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('id', $entity_type_id . '.' . $machine_name)
      ->execute();
  }

  /**
   * Helper function to ensure all required display modes for an entity type.
   *
   * @param \Drupal\Core\Entity\ContentEntityTypeInterface $entity_type
   *   The entity type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function ensureDisplayModes(ContentEntityTypeInterface $entity_type): void {
    foreach (['landscape', 'portrait'] as $type) {
      $machine_name = 'digital_signage_' . $type;
      if ($this->existsDisplayMode($entity_type->id(), $machine_name)) {
        // Display mode already exists.
        continue;
      }
      $displayMode = EntityViewMode::create([
        'id' => $entity_type->id() . '.' . $machine_name,
        'label' => 'Digital signage ' . $type,
        'targetEntityType' => $entity_type->id(),
      ]);
      $displayMode->save();
    }
  }

}
