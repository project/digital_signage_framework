<?php

namespace Drupal\digital_signage_framework;

/**
 * Interface for weight objects.
 */
interface WeightInterface {

  /**
   * Returns the weight for the given priority.
   *
   * @param int $priority
   *   The priority.
   *
   * @return int
   *   The weight.
   */
  public function getWeightByPriority(int $priority): int;

}
