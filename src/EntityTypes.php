<?php

namespace Drupal\digital_signage_framework;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides services around entity types.
 */
class EntityTypes {

  public const EXCLUDED = [
    'aggregator_feed',
    'aggregator_item',
    'block_content',
    'comment',
    'commerce_product_variation_type',
    'contact_message',
    'content_moderation_state',
    'crop',
    'danse_event',
    'danse_notification',
    'danse_notification_action',
    'digital_signage_content_setting',
    'digital_signage_device',
    'digital_signage_schedule',
    'entity_embed_fake_entity',
    'helpdesk_issue',
    'menu_link_content',
    'onesignal_device',
    'paragraph',
    'paragraphs_library_item',
    'path_alias',
    'redirect',
    'shortcut',
    'smart_date_override',
    'smart_date_rule',
    'wayfinding',
    'webform_submission',
    'workspace',
  ];


  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Renderer constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Gets all entity type definitions.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   The list of all entity type definitions.
   */
  public function all(): array {
    return $this->entityTypeManager->getDefinitions();
  }

  /**
   * Gets all entity type IDs.
   *
   * @return string[]
   *   All entity type IDs.
   */
  public function allIds(): array {
    return array_keys($this->all());
  }

  /**
   * Gets all entity type IDs that are not ignored.
   *
   * @return string[]
   *   All entity type IDs that are not ignored.
   */
  public function allEnabledIds(): array {
    return array_diff($this->allIds(), $this->allDisabledIds());
  }

  /**
   * Get all ignored entity type IDs.
   *
   * @return string[]
   *   All ignored entity type IDs.
   */
  public function allDisabledIds(): array {
    // @todo Allow other modules to alter this list.
    return self::EXCLUDED;
  }

}
