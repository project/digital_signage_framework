<?php

namespace Drupal\digital_signage_framework\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\digital_signage_framework\Entity\Device;
use Drupal\digital_signage_framework\Query;
use Drupal\digital_signage_framework\ScheduleManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Enable or disable emergency mode on devices.
 */
abstract class ActionBase extends ConfirmFormBase {

  /**
   * The temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected PrivateTempStoreFactory $tempStoreFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The schedule plugin manager.
   *
   * @var \Drupal\digital_signage_framework\ScheduleManager
   */
  protected ScheduleManager $scheduleManager;

  /**
   * The list of devices.
   *
   * @var \Drupal\digital_signage_framework\DeviceInterface[]
   */
  protected array $devices;

  /**
   * The query service.
   *
   * @var \Drupal\digital_signage_framework\Query
   */
  protected Query $queryService;

  /**
   * Constructs a new EmergencyMode confirm form.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The temp store factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\digital_signage_framework\ScheduleManager $schedule_manager
   *   The schedule plugin manager.
   * @param \Drupal\digital_signage_framework\Query $query_service
   *   The query service.
   */
  final public function __construct(PrivateTempStoreFactory $temp_store_factory, EntityTypeManagerInterface $entity_type_manager, ScheduleManager $schedule_manager, Query $query_service) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->scheduleManager = $schedule_manager;
    $this->queryService = $query_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ActionBase {
    return new static(
      $container->get('tempstore.private'),
      $container->get('entity_type.manager'),
      $container->get('schedule.manager.digital_signage_platform'),
      $container->get('digital_signage_content_setting.queries')
    );
  }

  /**
   * Gets the form ID.
   *
   * @return string
   *   The form ID.
   */
  abstract protected function id(): string;

  /**
   * {@inheritdoc}
   */
  final public function getFormId(): string {
    return $this->id() . '_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('entity.digital_signage_device.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $current_user_id = $this->currentUser()->id();
    if ($id = $this->getRequest()->get('entity_id')) {
      $this->tempStoreFactory->get($this->id())->set((string) $current_user_id, [Device::load($id)]);
      $form['entity_id'] = ['#type' => 'hidden', '#value' => $id];
    }
    $form = parent::buildForm($form, $form_state);
    $this->devices = $this->tempStoreFactory
      ->get($this->id())
      ->get((string) $current_user_id);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $current_user_id = $this->currentUser()->id();
    $this->devices = $this->tempStoreFactory
      ->get($this->id())
      ->get((string) $current_user_id);
    $this->tempStoreFactory
      ->get($this->id())
      ->delete((string) $current_user_id);
    if ($id = $form_state->getValue('entity_id')) {
      $form_state->setRedirect('entity.digital_signage_device.canonical', [
        'digital_signage_device' => $id,
      ]);
    }
    else {
      $form_state->setRedirectUrl($this->getCancelUrl());
    }
  }

}
