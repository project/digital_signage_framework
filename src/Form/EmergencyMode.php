<?php

namespace Drupal\digital_signage_framework\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\digital_signage_framework\Emergency;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Enable or disable emergency mode on devices.
 */
class EmergencyMode extends ActionBase {

  /**
   * The emergency services.
   *
   * @var \Drupal\digital_signage_framework\Emergency
   */
  protected Emergency $emergency;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): EmergencyMode {
    /** @var \Drupal\digital_signage_framework\Form\EmergencyMode $instance */
    $instance = parent::create($container);
    $instance->emergency = $container->get('digital_signage_content_setting.emergency');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function id(): string {
    return 'digital_signage_device_emergency_mode';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Configure emergency mode on selected devices');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Set emergency mode');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $default_mode = FALSE;
    $default_entity = '';
    if (count($this->devices) === 1) {
      $device = reset($this->devices);
      if ($entity = $device->getEmergencyEntity()) {
        $default_mode = TRUE;
        $default_entity = implode('/', [$entity->getReverseEntityType(), $entity->getReverseEntityId()]);
      }
    }
    $form['emergencymode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable emergency mode'),
      '#default_value' => $default_mode,
    ];
    $form['entity'] = [
      '#type' => 'select',
      '#title' => $this->t('Content'),
      '#default_value' => $default_entity,
      '#options' => $this->emergency->allForSelect(),
      '#states' => [
        'visible' => [
          ':input[name="emergencymode"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    if ($form_state->getValue('confirm')) {
      if ($form_state->getValue('emergencymode')) {
        [$type, $id] = explode('/', $form_state->getValue('entity'));
        /** @var \Drupal\Core\Entity\ContentEntityInterface|null $entity */
        $entity = $this->entityTypeManager->getStorage($type)->load($id);
        if ($entity) {
          $target_id = $entity->get('digital_signage')->getValue()[0]['target_id'];
          foreach ($this->devices as $device) {
            $device
              ->set('emergency_entity', $target_id)
              ->save();
          }
        }
      }
      else {
        foreach ($this->devices as $device) {
          $device
            ->set('emergency_entity', NULL)
            ->save();
        }
      }
    }
  }

}
