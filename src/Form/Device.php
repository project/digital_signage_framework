<?php

namespace Drupal\digital_signage_framework\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the device entity edit forms.
 */
class Device extends ContentEntityForm {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected Renderer $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): Device {
    /** @var \Drupal\digital_signage_framework\Form\Device $instance */
    $instance = parent::create($container);
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Exception
   */
  public function save(array $form, FormStateInterface $form_state): int {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => $this->renderer->render($link)];

    if ($result === SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New device %label has been created.', $message_arguments));
      $this->logger('device')->notice('Created new device %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The device %label has been updated.', $message_arguments));
      $this->logger('device')->notice('Updated new device %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.digital_signage_device.canonical', ['digital_signage_device' => $entity->id()]);

    return $result;
  }

}
