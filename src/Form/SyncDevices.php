<?php

namespace Drupal\digital_signage_framework\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\digital_signage_framework\PlatformPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form before clearing out the examples.
 */
class SyncDevices extends ConfirmFormBase {

  /**
   * The platform plugin manager.
   *
   * @var \Drupal\digital_signage_framework\PlatformPluginManager
   */
  protected PlatformPluginManager $pluginManager;

  /**
   * Constructs the form for synchronizing devices.
   *
   * @param \Drupal\digital_signage_framework\PlatformPluginManager $plugin_manager
   *   The platform plugin manager.
   */
  public function __construct(PlatformPluginManager $plugin_manager) {
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SyncDevices {
    return new SyncDevices(
      $container->get('plugin.manager.digital_signage_platform')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'digital_signage_device_sync_devices';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to sync all devices?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('entity.digital_signage_device.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->pluginManager->syncDevices();
    $this->messenger()->addStatus($this->t('Device synchronization completed!'));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
