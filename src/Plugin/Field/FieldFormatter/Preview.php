<?php

namespace Drupal\digital_signage_framework\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Preview' formatter.
 *
 * @FieldFormatter(
 *   id = "digital_signage_schedule_preview",
 *   label = @Translation("Preview"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class Preview extends FormatterBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected AccountProxy $currentUser;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected DateFormatter $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->currentUser = $container->get('current_user');
    $instance->dateFormatter = $container->get('date.formatter');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    // @todo There is be a better way of getting hold of the comparison values.
    // @phpstan-ignore-next-line
    return ($field_definition->toArray()['entity_type'] === 'digital_signage_device') && ($field_definition->getSetting('target_type') === 'digital_signage_schedule');
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    if (!$this->currentUser->hasPermission('digital signage framework access preview')) {
      return [];
    }
    /** @var \Drupal\digital_signage_framework\DeviceInterface $device */
    $device = $items->getEntity();
    $build = [
      '#prefix' => '<div class="digital-signage-device-preview-buttons">',
      '#suffix' => '</div>',
      '#attached' => [
        'drupalSettings' => [
          'digital_signage' => [
            'devices' => [
              $device->id() => [
                'orientation' => $device->getOrientation(),
                'proportion' => $device->getWidth() / $device->getHeight(),
                'schedule' => $device->getApiSpec(),
                'width' => $device->getWidth(),
                'height' => $device->getHeight(),
              ],
            ],
          ],
        ],
        'library' => [
          'digital_signage_framework/schedule.preview',
        ],
      ],
      [
        '#type' => 'button',
        '#value' => $this->t('Preview diagram'),
        '#attributes' => [
          'class' => ['digital-signage', 'diagram'],
          'device-id' => $device->id(),
          'stored-schedule' => 'false',
        ],
      ],
      [
        '#type' => 'button',
        '#value' => $this->t('Preview schedule'),
        '#attributes' => [
          'class' => ['digital-signage', 'preview'],
          'device-id' => $device->id(),
          'stored-schedule' => 'false',
        ],
      ],
      [
        '#type' => 'button',
        '#value' => $this->t('Preview slide'),
        '#attributes' => [
          'class' => ['digital-signage', 'slide'],
          'device-id' => $device->id(),
          'stored-schedule' => 'false',
        ],
      ],
    ];
    if ($schedule = $device->getSchedule()) {
      $live = [
        [
          '#type' => 'button',
          '#value' => $this->t('Live diagram'),
          '#attributes' => [
            'class' => ['digital-signage', 'diagram', 'live'],
            'device-id' => $device->id(),
            'stored-schedule' => 'true',
          ],
        ],
        [
          '#type' => 'button',
          '#value' => $this->t('Live schedule (@date)', [
            '@date' => $this->dateFormatter->format($schedule->getCreatedTime(), 'short'),
          ]),
          '#attributes' => [
            'class' => ['digital-signage', 'preview', 'live'],
            'device-id' => $device->id(),
            'stored-schedule' => 'true',
          ],
        ],
        [
          '#type' => 'button',
          '#value' => $this->t('Live slide'),
          '#attributes' => [
            'class' => ['digital-signage', 'slide', 'live'],
            'device-id' => $device->id(),
            'stored-schedule' => 'true',
          ],
        ],
        [
          '#type' => 'button',
          '#value' => $this->t('Live screenshot'),
          '#attributes' => [
            'class' => ['digital-signage', 'screenshot', 'live'],
            'device-id' => $device->id(),
          ],
        ],
        [
          '#type' => 'button',
          '#value' => $this->t('Live debug log'),
          '#attributes' => [
            'class' => ['digital-signage', 'debug-log', 'live'],
            'device-id' => $device->id(),
          ],
        ],
        [
          '#type' => 'button',
          '#value' => $this->t('Live error log'),
          '#attributes' => [
            'class' => ['digital-signage', 'error-log', 'live'],
            'device-id' => $device->id(),
          ],
        ],
      ];
      foreach ($live as $item) {
        $build[] = $item;
      }

    }
    return $build;
  }

}
