<?php

namespace Drupal\digital_signage_framework;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Provides emergency services.
 */
class Emergency {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Renderer constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Gets all published emergency entities.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   The list of emergency entities.
   */
  protected function queryAll(): array {
    $entities = [];
    try {
      $ids = $this->entityTypeManager
        ->getStorage('digital_signage_content_setting')
        ->getQuery()
        ->accessCheck(FALSE)
        ->condition('status', 1)
        ->condition('emergencymode', 1)
        ->execute();
      /** @var int[] $ids */
      array_walk($ids, static function (&$item) {
        $item = (int) $item;
      });
      /** @var \Drupal\digital_signage_framework\ContentSettingInterface $item */
      foreach ($this->entityTypeManager->getStorage('digital_signage_content_setting')->loadMultiple($ids) as $item) {
        /** @var \Drupal\Core\Entity\ContentEntityInterface|null $entity */
        $entity = $this->entityTypeManager->getStorage($item->getReverseEntityType())->load($item->getReverseEntityId());
        if (($entity instanceof FieldableEntityInterface) && (!$entity->hasField('status') || $entity->get('status')->value)) {
          $entities[] = $entity;
        }
      }
    }
    catch (PluginException) {
      // @todo Log this exception.
    }
    return $entities;
  }

  /**
   * Gets a list of emergency entities as sequence items.
   *
   * @return array
   *   The list of emergency entities as sequence items.
   */
  public function all(): array {
    $entities = [];
    foreach ($this->queryAll() as $entity) {
      $entities[] = (new SequenceItem(
        $entity->get('digital_signage')->getValue()[0]['target_id'],
        $entity->id(),
        $entity->getEntityTypeId(),
        $entity->bundle(),
        $entity->label(),
        0,
        FALSE
      ))->toArray();
    }
    return $entities;
  }

  /**
   * Gets a list of emergency entities as a form select list.
   *
   * @return array
   *   The list of emergency entities as form select list.
   */
  public function allForSelect(): array {
    $entities = [];
    foreach ($this->queryAll() as $entity) {
      $entities[implode('/', [$entity->getEntityTypeId(), $entity->id()])] = $entity->label();
    }
    return $entities;
  }

}
