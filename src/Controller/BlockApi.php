<?php

namespace Drupal\digital_signage_framework\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\block\BlockViewBuilder;
use Drupal\block\Entity\Block;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the Block controller.
 */
class BlockApi implements ContainerInjectionInterface {

  /**
   * The core renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The block view builder.
   *
   * @var \Drupal\block\BlockViewBuilder
   */
  protected BlockViewBuilder $blockViewBuilder;

  /**
   * Constructs the BlockApi object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The core renderer.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RendererInterface $renderer) {
    $builder = $entityTypeManager->getViewBuilder('block');
    if ($builder instanceof BlockViewBuilder) {
      $this->blockViewBuilder = $builder;
    }
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): BlockApi {
    return new BlockApi(
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * Handles the request.
   *
   * @param string $id
   *   The block ID.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function request(string $id): Response {
    if ($block = Block::load($id)) {
      $build = $this->blockViewBuilder->view($block);
      $build['#cache'] = ['max-age' => 0];
      $content = $this->renderer->renderPlain($build);
    }
    else {
      $content = '';
    }
    return new Response($content);
  }

}
