<?php

namespace Drupal\digital_signage_framework;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\digital_signage_framework\Annotation\DigitalSignagePlatform;
use Psr\Log\LoggerInterface;

/**
 * DigitalSignagePlatform plugin manager.
 */
class PlatformPluginManager extends DefaultPluginManager {

  /**
   * The array of known platform plugins.
   *
   * @var array|null
   */
  private static ?array $plugins = NULL;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Constructs DigitalSignagePlatformPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, LoggerInterface $logger) {
    $this->logger = $logger;
    parent::__construct(
      'Plugin/DigitalSignagePlatform',
      $namespaces,
      $module_handler,
      PlatformInterface::class,
      DigitalSignagePlatform::class
    );
    $this->alterInfo('digital_signage_platform_info');
    $this->setCacheBackend($cache_backend, 'digital_signage_platform_plugins', ['digital_signage_platform_plugins']);
  }

  /**
   * {@inheritdoc}
   */
  public function clearCachedDefinitions(): void {
    parent::clearCachedDefinitions();
    self::$plugins = NULL;
  }

  /**
   * Find and initialize all platform plugins.
   *
   * @return PlatformInterface[]
   *   The list of initialized platform plugins.
   */
  public function getAllPlugins(): array {
    if (self::$plugins === NULL) {
      self::$plugins = [];
      foreach ($this->getDefinitions() as $definition) {
        try {
          self::$plugins[] = $this->createInstance($definition['id']);
        }
        catch (PluginException) {
          // Ignore this.
        }
      }
      $this->logger->debug('Found %count platforms.', ['%count' => count(self::$plugins)]);
    }
    return self::$plugins;
  }

  /**
   * Sync devices of all enabled platforms.
   *
   * @param string|null $platform
   *   The platform.
   */
  public function syncDevices(?string $platform = NULL): void {
    try {
      $plugins = ($platform === NULL) ?
        $this->getAllPlugins() :
        [$this->createInstance($platform)];
      foreach ($plugins as $plugin) {
        $plugin->syncDevices();
      }
    }
    catch (PluginException | EntityStorageException) {
      // @todo write a log entry.
    }
  }

  /**
   * Push the current schedule to a device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   * @param bool $debug
   *   Flag, whether the device should be set into debug mode.
   * @param bool $reload_assets
   *   Flag, whether the device should reload assets.
   * @param bool $reload_content
   *   Flag, whether the device should reload the content.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function pushSchedule(DeviceInterface $device, bool $debug = FALSE, bool $reload_assets = FALSE, bool $reload_content = FALSE): void {
    /** @var \Drupal\digital_signage_framework\PlatformInterface $plugin */
    $plugin = $this->createInstance($device->bundle());
    $plugin->pushSchedule($device, $debug, $reload_assets, $reload_content);
  }

  /**
   * Push configuration to a device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   * @param bool $debug
   *   Flag, whether the device should be set into debug mode.
   * @param bool $reload_schedule
   *   Flag, whether the device should reload the schedule.
   * @param bool $reload_assets
   *   Flag, whether the device should reload assets.
   * @param bool $reload_content
   *   Flag, whether the device should reload the content.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function pushConfiguration(DeviceInterface $device, bool $debug, bool $reload_schedule, bool $reload_assets, bool $reload_content): void {
    /** @var \Drupal\digital_signage_framework\PlatformInterface $plugin */
    $plugin = $this->createInstance($device->bundle());
    $plugin->pushConfiguration($device, $debug, $reload_schedule, $reload_assets, $reload_content);
  }

}
